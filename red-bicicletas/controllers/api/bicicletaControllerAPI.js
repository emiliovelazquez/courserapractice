const { response } = require("express");
var Bicicleta = require("../../models/bicicleta");


exports.bicicleta_list = function(request, response){
    response.status(200).json({
        bicicletas : Bicicleta.todasLasBicicletas
    });
}

exports.bicicleta_create = function(request, response){
    var bicicleta = new Bicicleta(request.body.id, request.body.color, request.body.modelo);
    bicicleta.ubicacion = [request.body.lat, request.body.lng];

    Bicicleta.add(bicicleta);

    response.status(200).json({
        bicicleta: bicicleta
    })
}

exports.bicicleta_delete = function(request, response){
    Bicicleta.removeById(request.body.id);

    response.status(204).send();
}