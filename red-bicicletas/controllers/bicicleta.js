var Bicicleta = require("../models/bicicleta");


exports.bicicleta_list = function(request, response){
    response.render('bicicletas/index', {bicis: Bicicleta.todasLasBicicletas});
}

exports.bicicleta_create_get = function (request, response) {
    response.render('bicicletas/create');
}

exports.bicicleta_create_post = function (request, response) {
    var bicicleta = new Bicicleta(request.body.id, request.body.color, request.body.modelo);
    bicicleta.ubicacion = [request.body.lat, request.body.lng];

    Bicicleta.add(bicicleta);

    response.redirect('/bicicletas');
}

exports.bicicleta_update_get = function (request, response) {
    var bicicleta = Bicicleta.findById(request.params.id);

    response.render('bicicletas/update', {bicicleta});
}

exports.bicicleta_update_post = function (request, response) {
    var bicicleta = Bicicleta.findById(request.params.id);
    bicicleta.id = request.body.id;
    bicicleta.color = request.body.color;
    bicicleta.modelo = request.body.modelo;
    bicicleta.ubicacion = [request.body.lat, request.body.lng];

    response.redirect('/bicicletas');
}


exports.bicicleta_delete_post = function(request, response){
    Bicicleta.removeById(request.body.id);

    response.redirect('/bicicletas');
}