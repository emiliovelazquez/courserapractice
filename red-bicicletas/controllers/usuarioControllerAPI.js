var Usuario =  require('../../models/usuario');


exports.usuario_list = function(req, response){
    Usuario.find({}, function(err, usuarios){
        response.status(200).json({
            usuarios:usuarios
        });
    });
};

exports.usuarios_create = function(req, response){
    var usuario = new Usuario({nombre: req.body.nombre});

    usuario.save(function(err){
        response.status(200).json(usuario);
    });
};

exports.usuario_reservar = function(req, response){
    Usuario.findById(req.body.id, function(err, usuario){
        console.log(usuario);
        usuario.reservar(req.body.bici_id, req.body.desde, req.body.hasta, function(err){
            console.log('Reserva!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!');
            response.status(200).send();
        });
    });
};