var mongoose = require('mongoose');
var Schema = mongoose.Schema;

var bicicletasSchema = new Schema({
    code : Number,
    color: String,
    modelo : String,
    ubicacion : {
        type: [Number], index: {type: '2dsphere', sparse: true}
    }
});

bicicletasSchema.statics.createInstance = function(code, color, modelo, ubicacion){
    return new this({
        code: code,
        color: color,
        modelo: modelo,
        ubicacion: ubicacion
    });
};

bicicletasSchema.methods.toString = function() {
    return 'code: ' + this.code + ' | color: ' + this.color;
};

bicicletasSchema.statics.todasLasBicicletas = function(cb){
    return this.find({}, cb);
};


// var Bicicleta = function(id, color, modelo, ubicacion){
    //     this.id = id;
    //     this.color =color;
    //     this.modelo =modelo;
    //     this.ubicacion = ubicacion;
    // }
    
Bicicleta.prototype.toString = function(){
    return "ID: " + this.id + " | Color: " + this.color;
};

bicicletasSchema.statics.add = function(aBici, cb){
    this.create(aBici, cb);
};

bicicletasSchema.statics.findByCode = function(aCode, cb){
    return this.findOne({code: aCode}, cb);
};

module.exports = mongoose.model('Bicicleta', bicicletasSchema);
    // Bicicleta.todasLasBicicletas = [];
// Bicicleta.add = function(pBicicleta){
//     Bicicleta.todasLasBicicletas.push(pBicicleta);
// }

// Bicicleta.findById = function(pBiciId){
//     var bici = Bicicleta.todasLasBicicletas.find(x => x.id == pBiciId);
//     if (bici) {
//         return bici;
//     }else{
//         throw new Error(`No existe una bicicleta con el ID ${pBiciId}`);
//     }
// }

// Bicicleta.removeById = function(pBicicId){
//     for (var i = 0; i < Bicicleta.todasLasBicicletas.length; i++) {
//         if (Bicicleta.todasLasBicicletas[i].id == pBicicId) {
//             Bicicleta.todasLasBicicletas.splice(i, 1);
//             break
//         }
//     }
// }

// var bici1 = new Bicicleta(1, 'rojo', 'urbana', [37.7715666, -122.4318267])
// var bici2 = new Bicicleta(2, 'blanco', 'urbana', [37.7725966, -122.4318300])
// var bici3 = new Bicicleta(3, 'negro', 'urbana', [37.7735366, -122.4318350])
// var bici4 = new Bicicleta(4, 'azul', 'urbana', [37.7746066, -122.4318422])

// Bicicleta.add(bici1);
// Bicicleta.add(bici2);
// Bicicleta.add(bici3);
// Bicicleta.add(bici4);

// module.exports = Bicicleta;