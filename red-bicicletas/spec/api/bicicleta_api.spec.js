var mongoose = require('mongoose');
var Bicicleta = require("../../models/bicicleta");
var request = require("request");
var server = require("../../bin/www");

var base_url = "https://localhost:5000/api/bicicletas";

describe("Bicicleta API", ()=> {
    beforeEach(function (done){
        var mongoDB = 'mongoDB://localhost/testdb';
        mongoose.connect(mongoDB, {useNewUrlParser : true});

        const db = mongoose.connection;
        db.on('error', console.error.bind(console, 'connection error'));
        db.once('open', function(){
            console.log('we are connected to test database!');

            done();
        });
    });

    afterEach(function(done) {
        Bicicleta.deleteMany({}, function(err, success){
            if (err) console.log(err);
            done();
        });
    });

    describe("GET BICICLETAS /", ()=>{
        it('Satus 200', (done) =>{
            request.get(base_url, function(error, response, body){
                var result = JSON.parse(body);
                expect(response.statusCode).toBe(200);
                expect(result.Bicicleta.length).toBe(0);
                done();
            });
        });
    });

    describe("POST BICICLETAS /create", ()=>{
        it("Status 200", (done) =>{
            var headers = {'content-type' : 'application/json'};
            var aBici = '{"code": 10, "color": "rojo", "modelo": "urbana", "lat": 37.7735366, "lng": -122.4318350}';
            request.post({
                headers: headers,
                url: base_url + '/create',
                body: aBici},
                function(error, response, body){
                    expect(response.statusCode).toBe(200);
                    var bici = JSON.parse(body).bicicleta;
                    console.log(bici);
                    expect(bici.color).toBe("rojo");
                    expect(bici.ubicacion[0]).toBe(37.7735366);
                    expect(bici.ubicacion[1]).toBe(-122.4318350);
                    done();
                });
        });
    });
});


// // Resetea la lista de bicicletas antes de cada prueba
// beforeEach( () => {
//     Bicicleta.todasLasBicicletas = [];
//     console.log("testeando...");
// });

// describe('Bicicleta API', () =>{
//     describe("GET Bicicletas /", () =>{
//         it('Status 200', ()=> {
//             expect(Bicicleta.todasLasBicicletas.length).toBe(0);
            
//             var bici1 = new Bicicleta(1, 'rojo', 'urbana', [37.7715666, -122.4318267])
//             Bicicleta.add(bici1);

//             request.get("http://localhost:3000/api/bicicletas", function(error, response, body){
//                 expect(response.statusCode).toBe(200);
//             });
//         });
//     });

//     describe('POST Bicicletas /create', () =>{
//         it('Status 200', (done) =>{
//             var headers = {'content-type' : 'application/json'};
//             var nuevaBici = '{ "id": 10, "color": "rojo", "modelo": "urbana", "lat": -34, "lng": -54 }';
//             request.post({
//                 headers: headers,
//                 url: 'http://localhost:3000/api/bicicletas/create',
//                 body: nuevaBici
//             }, function(error, response, body) {
//                 expect(response.statusCode).toBe(200);
//                 expect(Bicicleta.findById(10).color).toBe("rojo");
//                 done();
//             })
//         });
//     });
// });