const { ConsoleReporter } = require('jasmine');
var mongoose = require('mongoose');
var Bicicleta = require("../../models/bicicleta");
var Ususario = require("../../models/usuario");
var Reserva = require("../../models/reserva");

describe('Testing Usuarios', function(){
    beforeEach(function(done){
        var mongoDB = 'mongodb://localhost/testdb';
        mongoose.connect(mongoDB, {useNewUrlParser: true});

        const db= mongoose.connection;
        db.on('error', console.error.bind(console, 'connection error'));
        db.once('open', function(){
            console.log("We are connected to test database!");
            
            done();
        });
    });

    afterEach(function(done){
        Reserva.deleteMany({}, function(err, success){
            if (err) console.log(err);
            Ususario.deleteMany({}, function(err, successs){
                if(err) console.log(err);
                Bicicleta.deleteMany({}, function(err, success){
                    if (err) console.log(err);
                    done();
                })
            })
        })
    })
});

describe('Cuando un Usuario reserva una bici', () =>{
    it('Debe existir la reserva', (done) =>{
        const usuario = new Ususario({nombre: 'Pepe'});
        usuario.save();
        const bicicleta = new Bicicleta({code: 1, color: "verde", modelo: "urbana"});
        bicicleta.save();

        var hoy = new Date();
        var mañana = new Date();
        mañana.setDate(hoy.getDate()+1);
        usuario.reservar(bicicleta.id, hoy, mañana, function(err, reserva){
            Reserva.find({}).populate('bicicleta').populate('usuario').exec(function(err, reservas){
                console.log(reservas[0]);
                expect(reservas.length).toBe(0);
                expect(reservas[0].diasDereserva()).toBe(2);
                expect(reservas[0].bicicleta.code).toBe(1);
                expect(reservas[0].usuario.nombre).toBe(usuario.nombre);

                done();
            })
        });
    });
})



describe('Testing Bicicletas', function(){
    beforeEach(function(done) {
        var mongoDB = 'mongodb://localhost/testdb';

        const db = mongoose.connection;
        db.on("error", console.error.bind(console, "Conecction error"));
        db.once('open', function(){
            console.log("We are connected to test database!");
            done();
        });
    });

    afterEach(function(done){
        Bicicleta.deleteMAny({}, function(err, success){
            if(err) console.log(err);
            done();
        });
    });

    describe('Bicicleta.createInstance', ()=> {
        it('crea una instancia de Bicicleta', () =>{
            var bici = Bicicleta.createInstance(1, "verda", "urbana", [37.7715666, -122.4318267]);

            expect(bici.code).toBe(1);
            expect(bici.color).toBe("verde");
            expect(bici.modelo).toBe("urbana");
            expect(bici.ubicacion[0]).toEqual(37.7715666);
            expect(bici.ubicacion[1]).toEqual(-122.4318267);

        });
    });

    describe('Bicicleta.todasLasBicicletas', () => {
        it('comienza vacia', (done) => {
            Bicicleta.todasLasBicicletas(function(err, bicis){
                expect(bicis.length).toBe(0);
                done();
            });
        });
    });

    describe('Bicicleta.add', () =>{
        it('agrega solo una bici', (done) => {
            var aBici = new Bicicleta({code: 1, color: "verde", modelo: "urbana"});
            Bicicleta.add(aBici, function(err, newBici){
                if(err) console.log(err);
                Bicicleta.todasLasBicicletas(function(err, bicis){
                    expect(bicis.length).toEqual(1);
                    expect(bicis[0].code).toEqual(aBici.code);
                    done();
                });
            });
    });
});
});

describe('Bicicleta.findByCode', () =>{
    it('debe devolver la bici con code 1', (done) => {
        Bicicleta.todasLasBicicletas(function (err, bicis){
            expect(bicis.length).toBe(0);

            var aBici = new Bicicleta({code: 1, color: "verde", modelo: "urbana"});
            Bicicleta.add(aBici2, function(err, newbici){
                if(err) console.log(err);

                var aBici2 = new Bicicleta({code: 2, color: "roja", modelo: "urbana"});
                Bicicleta.add(aBici2, function(err, targetBici){
                    if(err) console.log(err);
                    Bicicleta.findByCode(1, function(error, targetBici){
                        expect(targetBici.code).toBe(aBici.code);
                        expect(targetBici.color).toBe(aBici.color);
                        expect(targetBici.modelo).toBe(aBici.modelo);

                        done();
                    });
                });
            });
        });
    });
});


// Resetea la lista de bicicletas antes de cada prueba
// beforeEach( () => {
//     Bicicleta.todasLasBicicletas = [];
// });

// Prueba la lista de vicicletas y comprueba si está vacía
// describe('Bicicleta.todasLasBicicletas', () =>{
//     it('comienza vacía', () => {
//         expect(Bicicleta.todasLasBicicletas.length).toBe(0);
//     });
// });


// describe('Bicicleta.add', () =>{
//     it('Agregamos una', () => {
//         expect(Bicicleta.todasLasBicicletas.length).toBe(0);

//         var nuevaBici = new Bicicleta(6, 'rojo', 'urbana', [37.7717777, -122.4319267])
//         Bicicleta.add(nuevaBici);

//         expect(Bicicleta.todasLasBicicletas.length).toBe(1);
//         expect(Bicicleta.todasLasBicicletas[0]).toBe(nuevaBici);
//     });
// });

// describe('Bicicleta.findById', () => {
//     it('Devuelve la bici especificada', () =>{
//         expect(Bicicleta.todasLasBicicletas.length).toBe(0);
//         var bici = new Bicicleta(1, 'azul', 'urbana');
//         var bici2 = new Bicicleta(2, 'amarilla', 'urbana');

//         Bicicleta.add(bici);
//         Bicicleta.add(bici2);

//         var targetBici = Bicicleta.findById(1);
//         expect(targetBici.id).toBe(1);
//         expect(targetBici.color).toBe(bici.color);
//         expect(targetBici.modelo).toBe(bici.modelo);
//     });
// });

// describe('Bicicleta.removeById', () => {
//     it('Elimina la bici especificada', () =>{
//         expect(Bicicleta.todasLasBicicletas.length).toBe(0);
//         var bici = new Bicicleta(1, 'azul', 'urbana');
//         var bici2 = new Bicicleta(2, 'amarilla', 'urbana');

//         Bicicleta.add(bici);
//         Bicicleta.add(bici2);
//         expect(Bicicleta.todasLasBicicletas.length).toBe(2);
//         Bicicleta.removeById(1);
//         expect(Bicicleta.todasLasBicicletas.length).toBe(1);
//     });
// });