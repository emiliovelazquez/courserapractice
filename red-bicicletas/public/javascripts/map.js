var map = L.map('main_map').setView([37.7705066, -122.4318267], 41);


L.tileLayer('https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png', {
    attirubtion: '&copy: <a href= "https:/www.openstreetmap.org/copyright">OpenStreetMap</a> contributors'
}).addTo(map);

// L.marker([37.7705066, -122.4318267], 41).addTo(map);
// L.marker([37.7763434, -122.4347662], 73).addTo(map);

$.ajax({
    dataType: "json",
    url: "api/bicicletas",
    success: function(result){
        console.log(result);
        result.bicicletas.forEach(function(bicicleta) {
            L.marker(bicicleta.ubicacion, {title: bicicleta.id}).addTo(map);
        });
    }
})